//
//  MainViewController.swift
//  SpeechToTextPractice3
//
//  Created by 安井春輝 on 10/26/30 H.
//  Copyright © 30 Heisei haruki yasui. All rights reserved.
//

import UIKit
import Speech

class MainViewController: UIViewController {
    
    var mainView = MainView()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addSubview(mainView)
        mainView.frame = self.view.frame
        mainView.startAndStopRecognitonButton.addTarget(self, action: #selector(startAndStopRecognitoinButtonTapped), for: UIControl.Event.touchUpInside)
    }
    
    var isRecording = true
    @objc func startAndStopRecognitoinButtonTapped() {
        if isRecording {
            self.recordAndRecognitionSpeech()
            isRecording = false
            mainView.startAndStopRecognitonButton.backgroundColor = UIColor.red
        } else {
//            request.endAudio()
            audioEngine.stop()
            
//            audioEngine.inputNode.removeTap(onBus: 0)
            recognitionTask?.cancel()
            isRecording = true
            mainView.startAndStopRecognitonButton.backgroundColor = UIColor.gray
        }
        
    }
    
    //This will process the audio stream. It will give updates when the mic is receiving audio.
    //AVAudionEngine objects contains instances of various AVAudioNode subclasses.
    let audioEngine = AVAudioEngine()
    
    //This will do the actual speech recognition. It can fail to recognize speech and return nil, so it's best make it an optional.
    //By default, the speech recognizer will detect the device locale
    //and in response recognize the language appropriate to that geographical location.
    //So the default language can also be set by passing in a locale argument and identifier.
    //Like this: SFSpeechRecognizer(locale: Locale.init(identifier: "en-US"))
    let speechRecognizer: SFSpeechRecognizer? = SFSpeechRecognizer()
    
    //This allocates speech as the user speaks in real-time and controls the buffering.
    //If the audio was pre-recorded and stored in memory, you would use a SFSpeechURLRecognitionRequest instead.
    let request = SFSpeechAudioBufferRecognitionRequest()
    
    //This will be used to manage, cancel, or stop the current recognition task.
    var recognitionTask: SFSpeechRecognitionTask?
    
    func recordAndRecognitionSpeech() {
        
        //inputNode = The audio engine's singleton input audio node.
        //→音を聞き取るようのnodeの作成
        let node = audioEngine.inputNode
        
        let recordingFormat = node.outputFormat(forBus: 0)
        node.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { (buffer, _) in
            self.request.append(buffer)
        }
        
        audioEngine.prepare()
        do {
            try audioEngine.start()
        } catch {
            return print(error)
        }
        guard let myRecognizer = SFSpeechRecognizer() else {
            //A recognizer is not supported for the current locale
            return
        }
        if !myRecognizer.isAvailable {
            // A recognizer is not available right now.
            return
        }
        
        //Now the audio
        recognitionTask = speechRecognizer?.recognitionTask(with: request, resultHandler: { (result, error) in
            if let result = result {
                let bestString = result.bestTranscription.formattedString
                self.mainView.detectedTextLabel.text = bestString
                
                var lastString: String = ""
                for segment in result.bestTranscription.segments {
                    print("segment",segment)
                    let indexTo = bestString.index(bestString.startIndex, offsetBy: segment.substringRange.location)
                    print("indexTo",indexTo)
                    lastString = bestString.substring(from: indexTo)
                    print("lastString",lastString)
                }
                self.checkForColorsSaid(resultString: lastString)
            } else if let error = error {
                print(error)
            }
        })
        
        
    }
    
    func checkForColorsSaid(resultString: String) {
        switch resultString {
        case "red":
            mainView.colorView.backgroundColor = .red
        case "orange":
            mainView.colorView.backgroundColor = .orange
        case "yellow":
            mainView.colorView.backgroundColor = .yellow
        case "green":
            mainView.colorView.backgroundColor = .green
        case "blue":
            mainView.colorView.backgroundColor = .blue
        case "purple":
            mainView.colorView.backgroundColor = .purple
        case "black":
            mainView.colorView.backgroundColor = .black
        case "white":
            mainView.colorView.backgroundColor = .white
        case "gray":
            mainView.colorView.backgroundColor = .gray
        default: break
        }
    }
    


}

