//
//  MainView.swift
//  SpeechToTextPractice3
//
//  Created by 安井春輝 on 10/26/30 H.
//  Copyright © 30 Heisei haruki yasui. All rights reserved.
//

import UIKit

class MainView: UIView {
    
    let explanationWayLabel: UILabel = {
        let label = UILabel()
        label.text = "Tap button to start voice recognition!"
        label.textAlignment = .center
        return label
    }()
    
    //この記述はclosureを用いている。
    let startAndStopRecognitonButton: UIButton = {
        () -> UIButton in
        let button = UIButton()
        button.setTitle("Start/Stop", for: UIControl.State.normal)
        button.backgroundColor = UIColor.lightGray
        return button
    }()
    
    let detectedTextLabel: UILabel = {
        let label = UILabel()
        label.text = "Label"
        return label
    }()
    
    let sayColorNameLabel: UILabel = {
        let label = UILabel()
        label.text = "Say a color name!"
        return label
    }()
    
    let colorView: UIView = {
        let v = UIView()
        v.backgroundColor = UIColor.lightGray
        return v
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    func setupViews() {
        self.backgroundColor = UIColor.white
        
        self.addSubview(explanationWayLabel)
        self.addSubview(startAndStopRecognitonButton)
        self.addSubview(detectedTextLabel)
        self.addSubview(sayColorNameLabel)
        self.addSubview(colorView)
        
        explanationWayLabel.setAnchors(top: self.topAnchor, leading: nil, bottom: nil, trailing: nil, padding: UIEdgeInsets.init(top: 100, left: 0, bottom: 0, right: 0), size: CGSize.init(width: 400, height: 50))
        explanationWayLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        
        startAndStopRecognitonButton.setAnchors(top: explanationWayLabel.bottomAnchor, leading: nil, bottom: nil, trailing: nil, padding: UIEdgeInsets.init(top: 20, left: 0, bottom: 0, right: 0), size: CGSize.init(width: 100, height: 50))
        startAndStopRecognitonButton.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        
        detectedTextLabel.setAnchors(top: startAndStopRecognitonButton.bottomAnchor, leading: self.leadingAnchor, bottom: nil, trailing: self.trailingAnchor, padding: UIEdgeInsets.init(top: 20, left: 10, bottom: 0, right: 10), size: .init(width: 0, height: 300))
        
        sayColorNameLabel.setAnchors(top: detectedTextLabel.bottomAnchor, leading: nil, bottom: nil, trailing: nil, padding: UIEdgeInsets.init(top: 10, left: 0, bottom: 0, right: 0), size: CGSize.init(width: 150, height: 50))
        sayColorNameLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        
        colorView.setAnchors(top: sayColorNameLabel.bottomAnchor, leading: nil, bottom: nil, trailing: nil, padding: UIEdgeInsets.init(top: 10, left: 0, bottom: 0, right: 0), size: CGSize.init(width: 300, height: 100))
        colorView.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        
    }
    
    //In layoutSubviews() method, you can use frame after autolayout.
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension UIView {
    
    func setAnchors(top: NSLayoutYAxisAnchor?, leading: NSLayoutXAxisAnchor?, bottom: NSLayoutYAxisAnchor?, trailing: NSLayoutXAxisAnchor?, padding: UIEdgeInsets = .zero, size: CGSize = .zero) {
        
        self.translatesAutoresizingMaskIntoConstraints = false
        
        if let top = top {
            topAnchor.constraint(equalTo: top, constant: padding.top).isActive = true
        }
        
        if let leading = leading {
            leadingAnchor.constraint(equalTo: leading, constant: padding.left).isActive = true
        }
        
        if let bottom = bottom {
            bottomAnchor.constraint(equalTo: bottom, constant: padding.bottom).isActive = true
        }
        
        if let trailing = trailing {
            trailingAnchor.constraint(equalTo: trailing, constant: padding.right).isActive = true
        }
        
        if size.width != 0 {
            widthAnchor.constraint(equalToConstant: size.width).isActive = true
        }
        
        if size.height != 0 {
            heightAnchor.constraint(equalToConstant: size.height).isActive = true
        }
        
    }
}
